# williecadete-packer-rhel8-desktop

Packer templates for building base VM boxes for Virtualbox.

# Usage

## Installing Packer

To install packer, first find the appropriate package for your system and download it.

    https://www.packer.io/downloads.html

If you're using Homebrew

    $ brew tap homebrew/binary
    $ brew install packer

## Running Packer

    $ git clone https://gitlab.com/williecadete/packer-rhel8-desktop.git
    $ cd packer-rhel8-desktop
    $ packer build packer.json

## Supported versions

This templates was tested using a packer 1.4.3.
